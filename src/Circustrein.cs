﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CircustrainNS
{
    public class Circustrain
    {
        static void Main(string[] args)
        {

            //introduction to the programs. Asks user for input
            Console.WriteLine("Welcome to Circustrain. Please enter all the animals you want to take.");
            Console.WriteLine("(1,3,5) = (small, medium, large), (C,H) = (carnivore, herbivore)");
            Console.WriteLine("enter size, then meal preference. E.g. 5C = large carnivore");

            int[] animals = ReadInput();
            List<List<string>> Train = CreateTrain(animals);
            PrintTrain(Train);

        }

        public static void PrintTrain(List<List<string>> train)
        {
            foreach (List<string> wagon in train)
            {
                foreach (string animal in wagon)
                {
                    Console.Write(animal + ",");
                }
                Console.WriteLine("\n");
            }
        }

        /**
        * Creates an array animals from the input given by the user
        * @return animals
        **/
        public static int[] ReadInput()
        {
            int[] animals = { 0, 0, 0, 0, 0, 0 };
            var input = Console.ReadLine();

            /*
            * While the input is neither 'q' or 'Q' add animals to int[] animals iff they are in the right format.
            * If not the right format, user is notified the animal is not valid
            */

            while (input != "q" && input != "Q")
            {
                switch (input)
                {
                    case "5C":
                        animals[0] += 1;
                        break;
                    case "3C":
                        animals[1] += 1;
                        break;
                    case "1C":
                        animals[2] += 1;
                        break;
                    case "5H":
                        animals[3] += 1;
                        break;
                    case "3H":
                        animals[4] += 1;
                        break;
                    case "1H":
                        animals[5] += 1;
                        break;
                    default:
                        Console.WriteLine("The animal you entered is not valid");
                        break;
                }

                Console.WriteLine("please enter an animal or press 'Q' for quit");
                input = Console.ReadLine();
            }
            return animals;
        }

        /**
        * Creates a train from a array of animals
        * @param animals the integer of how many animals are there
        * @return the Train 
        **/
        public static List<List<string>> CreateTrain(int[] animals)
        {
            var Train = new List<List<string>>();

            for (int i = 0; i < animals.Length; i++)
            {
                while (animals[i] != 0)
                {
                    if (i == 0)
                    {

                        /* Since the large Carnivores can only be in a wagon by themselves, we have to make a new wagon for each
                        *  Large Carnivore. Once we added one to a wagon, we will remove it from the total number of Large Carnivores
                        *  in the animal array.
                        */
                        List<string> wagon = new List<string>();
                        wagon.Add("5C");
                        Train.Add(wagon);
                        animals[i] -= 1;
                    }
                    else if (i == 1)
                    {
                        /*
                        * Medium Carnivores can only be in a wagon with large Herbivores. Therefor, once we add a Medium Carnivore
                        * to a wagon, we will also add a Large Herbivore if possible. If there are no Large Herbivores left, then
                        * the Medium Carnivores have to be in a wagon on their own.
                        */
                        List<string> wagon = new List<string>();
                        wagon.Add("3C");
                        animals[i] -= 1;

                        if (animals[3] != 0)
                        {
                            wagon.Add("5H");
                            animals[3] -= 1;
                        }
                        Train.Add(wagon);
                    }
                    else if (i == 2)
                    {
                        List<string> wagon = new List<string>();
                        wagon.Add("1C");
                        animals[i] -= 1;

                        if (animals[4] >= 3)
                        {
                            wagon.Add("3H");
                            wagon.Add("3H");
                            wagon.Add("3H");
                            animals[4] -= 3;
                        }
                        else
                        {
                            if (animals[3] != 0)
                            {
                                wagon.Add("5H");
                                animals[3] -= 1;

                                if (animals[4] != 0)
                                {
                                    wagon.Add("3H");
                                    animals[4] -= 1;
                                }
                            }
                            else
                            {
                                if (animals[4] != 0)
                                {
                                    wagon.Add("3H");
                                    animals[4] -= 1;
                                }
                                if (animals[4] != 0)
                                {
                                    wagon.Add("3H");
                                    animals[4] -= 1;
                                }
                            }

                        }
                        Train.Add(wagon);
                    }
                    else
                    {
                        List<string> wagon = new List<string>();
                        var weight = 0;

                        // If there are animals left, do the following
                        while (weight < 10 && (animals[3] > 0 || animals[4] > 0 || animals[5] > 0))
                        {
                            if (animals[3] > 0 && weight + 5 <= 10)
                            {
                                wagon.Add("5H");
                                weight += 5;
                                animals[3] -= 1;
                            }
                            else if (animals[4] > 0 && weight + 3 <= 10)
                            {
                                wagon.Add("3H");
                                weight += 3;
                                animals[4] -= 1;
                            }
                            else
                            {
                                wagon.Add("1H");
                                weight += 1;
                                animals[5] -= 1;
                            }
                        }
                        Train.Add(wagon);
                    }
                }
            }
            return Train;
        }
    }
}
