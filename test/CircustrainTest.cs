using System;
using Xunit;
using System.Collections.Generic;
using CircustrainNS;

namespace CircustrainTests
{
    public class CircustrainTest
    {

        /**
         * Runs the functions and gets the result from animals
         *
         * @param animals the integer of how many animals are there
         * @returns the Train
         */
        private List<List<String>> GetResult(int[] animals) 
        {
            return Circustrain.CreateTrain(animals);
        }

        /**
         * Compares two nested lists with each other; ensure every nested element is also the same
         *
         * @param list1 the first list to compare
         * @param list2 the second list to compare
         * @returns whether or not the lists are exactly the same
         */
        private bool CompareTwoNestedLists(List<List<String>> list1, List<List<String>> list2)
        {
            bool same = true;

            if (list1.Count == list2.Count) {
                for (int i = 0; i < list1.Count; i++) {
                    if (same) {
                        if (list1[i].Count == list2[i].Count) {
                            for (int j = 0; j < list2[i].Count; j++) {
                                if (list1[i][j] != list2[i][j]) {
                                    same = false;
                                    break;
                                }
                            }
                        } else {
                            return false;
                        }
                    } else {
                        break;
                    }
                }
            } else {
                return false;
            }

            return same;
        }

        /**
        * Checks whether the train is empty when input is empty
        **/
        [Fact]
        public void EmptyTrainOnNoInput()
        {
            //Arrange
            int[] animals = {0,0,0,0,0,0}; 
            //Act
            List<List<String>> Train = GetResult(animals);
            //Assert            
            Assert.True(Train.Count == 0);
        }

        /**
        * Checks whether the train is is not empty if an input is given
        **/
        [Fact]
        public void NonEmptyTrainOnAInput()
        {
            //Arrange
            int[] animals = {1,1,1,1,1,1}; 
            //Act
            List<List<String>> Train = GetResult(animals);
            //Assert            
            Assert.False(Train.Count == 0);
        }

        /**
        * Checks whether the same input creates the same train
        **/
        [Fact]
        public void SameTrainOnSameInput()
        {
            //Arrange
            int[] animals = {1,2,3,4,5,6};
            int[] animals2 = {1,2,3,4,5,6};
            
            //Act
            List<List<String>> Train = GetResult(animals);
            List<List<String>> Train2= GetResult(animals2);

            //Assert            
            Assert.True(CompareTwoNestedLists(Train, Train2));
        }

        /**
        * Checks whether a different input creates different train
        **/
        [Fact]
        public void DifferentTrainOnDifferentInput()
        {
            //Arrange
            int[] animals = {1,2,3,4,5,6};
            int[] animals2 = {6,5,4,3,2,1};
            
            //Act
            List<List<String>> Train = GetResult(animals);
            List<List<String>> Train2= GetResult(animals2);

            //Assert            
            Assert.False(CompareTwoNestedLists(Train, Train2));
        }

        /**
        * Checks whether Large Carnivores are placed in separate wagons
        **/
        [Fact]
        public void NoLargeCarnivoresTogether()
        {   
            //Arrange
            int[] animals = {100, 0, 0, 0, 0, 0};

            //Assert
            Assert.True(GetResult(animals).Count == 100);
        }

        /**
        * Checks whether Medium Carnivores are placed in separate wagons if no Large Herbivores are available
        **/
        [Fact]
        public void NoMediumCarnivoresTogether()
        {   
            //Arrange
            int[] animals = {0, 100, 0, 0, 0, 0};

            //Assert
            Assert.True(GetResult(animals).Count == 100);
        }

        /**
        * Checks whether Medium Carnivores are placed with Large Herbivores if available
        **/
        [Fact]
        public void MatchMediumCarnoviresWithLargeHerbivores()
        {   
            //Arrange
            int[] animals = {0, 100, 0, 100, 0, 0};

            //Assert
            Assert.True(GetResult(animals).Count == 100);
        }

        /**
        * Checks whether Small Carnivores are placed with Large and Medium Herbivores if available
        **/
        [Fact]
        public void MatchSmallCarnoviresWithLargeAndMediumHerbivores()
        {   
            //Arrange
            int[] animals = {0, 0, 10, 7, 6, 0};

            //Assert
            Assert.True(GetResult(animals).Count == 10);
        }

        /**
        * Checks whether the train can be filled with an assortment of herbivores
        **/
        [Fact]
        public void AssortmentOfHerbivoresInTrain()
        {   
            //Arrange
            int[] animals = {0, 0, 0, 4, 6, 7};

            //Assert
            Assert.True(GetResult(animals).Count == 5);
        }
        
        
        /**
        * Checks whether the train can be filled with an assortment of carnivores
        **/
        [Fact]
        public void AssortmentOfCarnivoresInTrain()
        {   
            //Arrange
            int[] animals = {5, 5, 5, 0, 0, 0};

            //Assert
            Assert.True(GetResult(animals).Count == 15);
        }

    }
}
